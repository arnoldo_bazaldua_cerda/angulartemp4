
import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';

    error: any;
  private autorized: boolean;
  constructor(public af: AngularFireAuth, public router: Router) {

  }


  signOut(): void {
    this.af.auth.signOut();
    this.router.navigate(['/'])
  }


}
