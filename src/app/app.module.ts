import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule, MatDialogModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http'; 

import { FormGroup, FormArray, FormBuilder, Validators, ReactiveFormsModule  } from '@angular/forms';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { AppRouting } from './app-routing.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { RegistrationComponent } from './registration/registration.component';
import { AboutComponent } from './about/about.component';
import { InfoComponent } from './info/info.component';
import { PackagesComponent } from './packages/packages.component';
import { ConfRegComponent } from './conf-reg/conf-reg.component';
import { SearchPanelComponent } from './search-panel/search-panel.component';

import { RegistrationService } from './registration/registration.service';

import { Observable } from 'rxjs/Observable';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
// Do not import from 'firebase' as you'd lose the tree shaking benefits

import { environment } from '../environments/environment';


/*export const firebase: {
   apiKey: 'AIzaSyAovlpoxsyTc-K1OOaj2ODJYMgOWV1v4R0',
    authDomain: 'zun-jobs.firebaseapp.com',
    databaseURL: 'https://zun-jobs.firebaseio.com',
    projectId: 'zun-jobs',
    storageBucket: 'zun-jobs.appspot.com',
    messagingSenderId: '679917365881'
    } */

@NgModule({
  declarations: [

    AppComponent,
    UsersComponent,
    LoginComponent,
    WelcomeComponent,
    NotfoundComponent,
    RegistrationComponent,
    AboutComponent,
    InfoComponent,
    PackagesComponent,
    ConfRegComponent,
    SearchPanelComponent
    
  ], 
   imports: [
    //MaterializeModule, 
      BrowserModule,
    AngularFireModule.initializeApp(environment.firebase,'my-app'),
    //AngularFireDatabaseModule,
   // AngularFireAuthModule,
    AngularFireAuthModule,
    HttpClientModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatDialogModule,    
    AppRouting,
    FormsModule,
    ReactiveFormsModule
    // other imports here
  ],
  providers: [RegistrationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
