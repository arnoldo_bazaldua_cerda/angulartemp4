import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { RegistrationComponent } from './registration/registration.component';
import { InfoComponent } from './info/info.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AboutComponent } from './about/about.component';
import { PackagesComponent } from './packages/packages.component';
import { ConfRegComponent } from './conf-reg/conf-reg.component';
import { SearchPanelComponent } from './search-panel/search-panel.component';

import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';

/*
edit   HomeComponent

*/
const routes:Routes =[
      { path: '', component: WelcomeComponent },
      { path: 'users', component: UsersComponent },
      { path: 'registration', component: RegistrationComponent },
      { path: 'packages', component: PackagesComponent },
      { path: 'info', component: InfoComponent },
      { path: 'about', component: AboutComponent },
      { path: 'login',      component: LoginComponent	 },
      { path: 'confirmation',      component: ConfRegComponent  },
      { path: 'search',      component: SearchPanelComponent  },
	{ path: '**', component: NotfoundComponent }

];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRouting{
	

}