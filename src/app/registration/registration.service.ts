
import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class RegistrationService {

  result: any;
  constructor(private http: HttpClient) {}


 findUser(/*name, price*/) {
    const uri = 'http://localhost:3000/users/{id}';
   /* const obj = {
      name: name,
      price: price
    };  */
    return this
      .http
      .get(uri)
     /* .subscribe(res =>
          console.log('Done' + res));*/
      .map(res => {
              return res;
            });    
  }


 addUsers(user) {
    const uri = 'http://localhost:3000/users';
    const userF = {
      fullname: user.fullname,
      email: user.email
    };  
    console.log(user)
    return this
      .http
      .post(uri,userF)
     /* .subscribe(res =>
          console.log('Done' + res));*/
      .map(res => {
              return res;
            });    
  }


   editUser(/*name, price*/) {
    const uri = 'http://localhost:3000/users';
   /* const obj = {
      name: name,
      price: price
    };  */
    return this
      .http
      .get(uri)
     /* .subscribe(res =>
          console.log('Done' + res));*/
      .map(res => {
              return res;
            });    
  }

  deleteUser(/*name, price*/) {
    const uri = 'http://localhost:3000/users';
   /* const obj = {
      name: name,
      price: price
    };  */
    return this
      .http
      .get(uri)
     /* .subscribe(res =>
          console.log('Done' + res));*/
      .map(res => {
              return res;
            });    
  }

  getUserByCat(/*name, price*/) {
    const uri = 'http://localhost:3000/users';
   /* const obj = {
      name: name,
      price: price
    };  */
    return this
      .http
      .get(uri)
     /* .subscribe(res =>
          console.log('Done' + res));*/
      .map(res => {
              return res;
            });    
  }

}
