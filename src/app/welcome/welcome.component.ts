import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})


export class WelcomeComponent implements OnInit {
  error: any;
  private autorized: boolean;
  constructor(public af: AngularFireAuth, public router: Router) {
    this.af.authState.subscribe(auth => {
      console.log(auth)
      if (auth) {
        this.autorized=true;
        this.router.navigateByUrl('/registration');
      } else {
        this.autorized=false;
      }
    });
  }

  ngOnInit() {}

  loginFb() {

    const provider = new firebase.auth.FacebookAuthProvider()

      this.af.auth.signInWithPopup(provider)
      .then((credential) =>  {
        console.log(credential);
      })
      .catch(error => console.log(error));

  }

  loginGog() {

    const provider = new firebase.auth.GoogleAuthProvider()

      this.af.auth.signInWithPopup(provider)
      .then((credential) =>  {
        console.log(credential);
      })
      .catch(error => console.log(error));

  }

}
