import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfRegComponent } from './conf-reg.component';

describe('ConfRegComponent', () => {
  let component: ConfRegComponent;
  let fixture: ComponentFixture<ConfRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
